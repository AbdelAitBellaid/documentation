# LE STYLING : Personnaliser BootsTrap

Dans Bootstrap, nous avons un catalogue d'éléments pour nous  
permettre de customiser notre page web.  
Cependant tout les éléments ne sont pas adapté, nous pouvons alors  
les personnaliser. ***On surcharge une feuille de style***

### SURCHARGER UNE FEUILLE DE STYLE
  
Pour **surcharger** la feuille de style de BT, on procède ainsi :  
Dans notre dossier ASSETS, on va ajouter un dossier STYLES, dans   
lequel nous mettrons un fichier *BootsTrap.css*.

>Exemple : Modifions la couleur d'un bouton.  
>*** Dans la console (sur la droite) on copie la ligne CSS du  
>bouton et nous la collons dans *BootsTrap.css*, puis on modifie les  
paramètres de celui-ci.***

ATTENTION:   
Si notre feuille de style personnalisé, vient avant la feuille originale  
de BT, elle ne sera pas pris en compte.  
En effet WS prendra la feuille de style la plus récente dans notre ligne de code.  
Donc dans le "HEAD", là ou l'inclusion de la "StyleSheet" de  
BootsTrap est inscrite, on ajoute plus bas : 
 
```<link rel="stylesheet"href="../assets/styles/bootstrap.css">```      


***ASTUCE: SHIFT + CTRL + V dans WebSTorm, nous indique 
la liste des"copié-collé" fait dans le projet*** 

Dans notre projet, pour une organisation optimale, on va créer une feuille 
CSS par éléments, et pour éviter d'avoir autant de ligne que de feuille, on utilise les IMPORTS.  

### LES IMPORTS

Dans un fichier BOOTSTRAP.CSS , on indique ce que l'on importe ainsi:  
 `@import"button-bootstrap.css"`  
 `@import"jumbotron-bootstrap.css`
 
 

