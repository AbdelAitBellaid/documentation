# Le MarkDown et Le GIT

Le MarkDown \(MD) est un language de prise de note, et + ou - de mise en forme de texte.
Pour faure un fichier propre, il faut l'organiser. \(Exemple du CV)

### MarkDown : La structure 

Sur une page il ne doit y avoir qu'un titre H1 ou # \(Curriculum Vitae de M...).
Les autres titres se déclinent en H2 ou ##,et dans chaque sous parties, on indiquera des H3 ou ###.
Dans les H3 ### si on y inscrit des informations, de type nom, date, etc... on les présentera en H5 ######.
Ceci est la structure, le squelette de notre document.

Pour faire un paragraphe explcatif p, en MD on n'utilise aucun marquage spécifique,
il suffit simplement d'écrire le paragraphe.

(Point important: le SEO = Search Engine Optimisation. Il s'agit de la structuration sémantique d'un travail, afin d'optimiser, 
et faciliter la visiblité sur les moteurs de recherche)

### MarkDown : Les BackTicks

Pour intégrer du CODE en MD (dans un encadré dédié) on utilise les BACKTICKS (```) CTRL + ALT + 7.
Il y a 2 types de Backticks:

**Les BackTicks simples** : `Ils mettent en surbrillance une ligne.`

et 

**Les Backtips triple** : Ils retrancrivent du code.

```javascript
<!DOCTYPE html>
 <html>
     <head>
         <meta charset="utf-8">
         <title>HTML basics</title>
     </head>
     <body>
 
 
     </body>
 </html>
```

### Le GIT

Le Git est un protocole qui permet de stocker des informations.
Pour accéder à ces infos, on a GITHUB et GITLAB, qui sont des sites internet qui utilise le GIT.

### Stocker son MD avec GITLAB

On se log sur GITLAB.
A partir du GITLAB, on fait un ***New Project***, que l'on nomme comme souhaité.
On rend notre travail visible, en cochant sur ***PUBLIC***.
On coche la case ***README***.
Il faut ensuite jumeler WS et GIT pour permettre le transfert des fichiers, en cliquant sur CLONE.
On copie le lien HTTPS.

Sur l'écran de démarrage de WebStorm, on clique sur ***CHECKOUT from version control***, puis sur ***GIT***.
On colle le lien HTTPS copié précedement sur GITLAB.
On appuie sur ***TEST*** (pour vérifier le login), puis sur ***CLONE***.

##### POUR SAUVEGARDER SON TRAVAIL:



Sur WS, on trouve en haut à droite 2 flèches:

- La flèche verte ***COMMIT***, sauvegarde sur l'ordinateur.
Dans l'encadré ***COMMIT MESSAGE***, on explique le contenu du document.
On décoche ***Perform code analysis*** et ***Check TODO***, puis on clique sur ***COMMIT***.

A savoir : Les couleurs bleu et vert des fichiers indiquent dans le premier cas, un fichier modifié, dans le deuxième cas un nouveau fichier.

Quand on clique sur ***COMMIT***, les couleurs disparaissent, cela indique la SAUVEGARDE.

( La flèche bleue permet de ***PULL***, et ramène les infos sauvegardées sur GITLAB.)

Pour faire un ***PUSH***, il faut aller sur VCS, puis sur GIT et faire un PUSH.

