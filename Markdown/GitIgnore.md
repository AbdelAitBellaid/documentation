# GITIGNORE
Il y a un certains nombre de fichiers cachés, notamment pour GIT,  
et si on modifie, ou supprime ces fichiers, l'intégrité de GIT et des fichiers  
qu'il contient sera impacté.

De même pour le fichier *.idea* qui contient les config de WS.  
Tout les fichier commençant par un "." sont des fichiers dit "*cachés*".  

Pour éviter ces désagréments on crée un fichier que l'on nomme ***GITIGNORE***

#### Créer un fichier GitIgnore:  
> New file (on nomme ce dossier avec un "." devant.)  
>> .GitIgnore  
WebStorm demande l'ajout à GIT, on indique "OUI".  
Dans ce dossier, on ajoute ce que l'on veut exclure du partage via GIT,  
en l'occurence ici le fichier *.idea* .

Pour créer un brouillon, et qu'il ne soit pas partagé avec GIT, on écrit son intitulé dans GitIgnore.  
La meilleur solution pour un brouillon, est d'utiliser ***un éditeur de texte***.

Dans le cas ou WS est paramètrer  pour toujours envoyer vers GIT, il faudra exclure le fichier manuellement.  
>>Dans la console de WS, on inscrit : git rm...-f 

***ASTUCE:***Pour une meilleur expèrience de travail sur WS, il faut utiliser le COMMIT souvent.
D'une part on sauvegarde notre travail, et d'autre part en cas d'erreur la correction sera plus simple qu'avec un seul gros dossier "commité".
