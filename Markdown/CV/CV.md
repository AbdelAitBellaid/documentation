# CURRICULUM VITAE

>Abdel Ait Bellaid  
39 ans  
27 Résidence les Taratres  
92500 Rueil Malmaison  
06.12.40.25.20
abdelaitbellaid@gmail.com

## ETUDES ET FORMATIONS  

**2003 - Niveau DEUG 2 Administration Economique et Sociale**  
**2000 - Baccalauréat Economiques et Sociales**

## LANGUES

- ANGLAIS : Niveau intermédiaire
- ESPAGNOL : Niveau scolaire
- ARABE Littéraire : Niveau scolaire
- ARABE : Langue maternelle

## EXPERIENCES PROFESSIONNELLES  

**2018 - Chargé de client Service Financier et Recouvrement**

- Gestion Portefeuille
- Recouvrement créances clients (Particuliers, Professionnelles , Administrations publiques )
- Facturation

## HOBBIES

**SPORTS** :  
- Kick Boxing 
- Boxe anglaise
- Crossfit
- Course à pied


  
