#BOOTSTRAP

*** A savoir: Un navigateur, est ce que l'on appelle un PARCEUR, il parcourt un fichier HTML, et en fonction des infos il execute des actions.***

Quand on crée un site web, il faut déterminer le nombre de page.  
En fonction de cela, on va créer autant de dossier.  
Ex: Homepage, About, FAQ, etc...  

Si notre site contient des img et logo, on crée un dossier **ASSETS**.  
Ex: 
> Assets
>> ==>img.  
>> ==>Vendors (Contient tout les scripts type CSS et HTML, de source externe)

*** A savoir: Pour améliorer le référencement , il y a 3 balises à connaitre:  

- `<section></section>`, elle délimite les différentes parties sur lapage web.  
====> Dans la `<section></section>`, il peut y avoir un `<header></header>`et un `<footer></footer>`. Elles s'utilisent comme des `<div>`.  
Elles permettent au moteur de recherche de comprendre qu'il y a des parties différenciées.***  

RAPPEL: Sur notre site web, il n'y a qu'un H1 par page. Google ce sert de ce H1 pour comprendre ce qu'il y a sur la page. 

###BOOTSTRAP,C'EST QUOI?  

Bootstrap est FRAMEWORK,  
comme Fundation et Stylus. Un outil de création (graphisme, animation et interactions avec la page dans le navigateur, etc.)
Il est disponible sur:  
'https://getbootstrap.com'

###UTILISATION  

Lorsqu'on utilise le framework BOOTSTRAP, il faut indiquer la ligne suivante dans le `<head></head>` 
  

```javascript
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
```  
```javascript
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
``` 
Cette façon de faire, n'est pas la meilleur, mais pour le début nous ferons de cette manière.

Dans cette partie 'https://getbootstrap.com/docs/4.3/components/alerts/' on trouve tout les gadgets qui permettent de customiser sa page web.  

###BOOTSTRAP: LA GRILLE (LAYOUT) 

'https://getbootstrap.com/docs/4.3/layout/overview/'

Ceci nous permet de faire une mise en page, qui rend notre page web **RESPONSIVE**.  
L'écran est divisé en 12 colonnes, et c'est sur cette base que la mise en page se fait.  

***A Savoir: Une page web qui utilise BOOTSTRAP doit être contenu dans une `<div>` dites "container".  
Elle placer au début du`<body>` et juste avant la fin `</body>`.  

```javascript
<div class = "container">...</div>
```
Cela permet de centrer la page et laisse toujours une marge à gauche et à droite,  
peut importe la taille de la fenêtre.

```javascript
<div class = "container-fluid">...</div>
```
Cette autre façon d'écrire le "container" permet d'occuper le maximum d'espace sur la page web,  
et ne laisse qu'***une goutière***de chaque côté.
 
***Important tout comme le H1, il ne doit y avoir qu'un seul "container" par page web.***

####ROW & COL

La page web se divise en lignes ***ROW*** et en colonnes ***COL***.  

Chaque `<section></section>` peut devenir une ligne, pour cela il faut attribuer une "class" ***ROW*** 

```javascript
<section class = row>...</section>  
```
`IMPORTANT:  
Chaque élément d'une page web peut être reconnu comme une ligne, en ajoutant une "class=row".`

Exemple: `<div class = row></div>`

IL N'Y A PAS DE "ROW" SANS "COL" et INVERSEMENT.

En l'occurence dans notre exemple :   

```javascript
<div class = row>
  <div class = "col-12">
...
  </div>
</div>
```
Le "col-12" indique que l'on souhaite que la ligne "row" occupe la totalité des 12 colonnes.

**Pour mettre les éléments sur la même ligne, on subdivise les 12 colonnes, en fonction de  l'espace attribué aux éléments.**

Exemple:

```javascript
<div class="row">
    <div class="col-4">
    ...
    </div>
    <div class="col-8">
    ...
    </div>
</div>
```

Cependant, en responsive, en fonction de la taille de la fenêtre, plus celle-ci sera petite, plus le résultat sera brouillon.
Pour éviter cela on va rajouter un paramètre au code, pour que les éléments s'organise automatiquement en fonction de la taille de la fenêtre.

Exemple:

```javascript
<div class="row">
    <div class="col-12 col-md-4">
    ...
    </div>
    <div class="col-12 col-md-8">
    ...
    </div>
</div>
```

***Il faut éviter de mettre des "COL" & "ROW" sur des balises de sémantique (section,header,body,...).
Il est préférable d'utiliser les balises de mise en forme comme "div"***

Pour centrer un élément sur une ligne "ROW" on ajoute au code un élément: 

```javascript
<div class="row justify-content-center">
              <div class="col-12 col-md-4">
              ...
              </div>
              <div class="col-12 col-md-8">
              ...
             </div>
</div>
```
